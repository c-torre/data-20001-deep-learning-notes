% This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

\documentclass[a4paper,twocolumn]{report}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{bm}

\author{Carlos de la Torre}
\date{May 2020}


\begin{document}
\chapter{Background and Fundamentals}

\begin{description}
  \item[Lecture time] 45 min
  \item[Source] Text
\end{description}
 
\section{Deep Learning}
Abstract and formal tasks are hard for humans but easy for computer, like playing chess.
These problems can be solved by \emph{rule-based systems}, which are hand-crafted by humans.
In classical computing paradigm, rules and data are used with a computer system to obtain answers.

However, tasks such as understanding human language or recognizing faces are intuitive for humans, but hard to formalize.
Therefore, they are difficult for computers.
In these cases, rules cannot be hand-crafted, they have to be learnt from data.
As a result, we move from classical computing to machine learning, in which data and answers are used together with a model to craft rules.

Deep learning is an attempt at solving these kind of ``unintuitive'' problems computationally.
As Goodfellow \textit{et al.} in the \textit{Deep Learning} textbook\footnote{\url{https://www.deeplearningbook.org}}:
\begin{quote}
  ``[Deep learning] allows computers to \emph{learn from experience} and understand the worls in terms of a \emph{hierarchy of concepts}, with \emph{each concept defined through its relation to simpler concepts}''
\end{quote}

But what is deep learning in relation with all these concepts of ``artificial intelligence'' or ``machine learning''?
At the most general level we have \emph{artificial intelligence}, such as knowledge bases.
Within, we have \emph{machine learning}, e.g.\ logistic regression.
Inside, we have \emph{representation learning}, e.g.\ shallow autoencoders.
Finally we reach the innermost layer inde all the previous ones: \emph{deep learning}, e.g.\ multilayer perceptrons.

If we attempt to make a figure of input, processing, and output from various computational approaches, we see differences between rule-based systems, classic machine learning, and representation learning (including deep learning).
In rule-based systems, input goes to a hand-designed program to produce output.
In contrast, in classic machine learning, input is used to produce hand-designed features, the system produces \emph{mapping from features}, to finally give an output.
For representation learning, we distingish between non-deep learning and deep learning methods.
In non-deep learning approaches, in input produces \emph{features} made by the system, which then give rise to \emph{mapping from features}, giving an output.
Finally in deep learning an input makes the system generate \emph{simple features}, for which additional layers add \emph{more abstract features}, producing \emph{mapping from features}, and finally giving an output.


\subsection{History of Deep Learning}
Deep learning has a long history with several peaks of activity and different names.

\subsubsection{1940s--1960s: Cybernetics}
At first theories were based on biological learning, and the perceptron model was developed.
Historically, computational models have attempted to mirror biological learning, and thus based on models of biological networks of neurons.
The model of the single neuron was developed in 1943 by McCulloch and Pitts.
In their model, we can imagine a neuron connected to several inputs, with three weights associated to their respective values.
The value of the neuron (function \(y\)) is given by:

\begin{equation*}
  y = \text{sgn}(\sum_{i} w_{i} x_{i})
\end{equation*}

In an example with three input neurons, they would carry three associated values and weights.
These would go through the described equation to give a single output: the value of our model neuron.
However there is an important issue with this model: it is linear.
Therefore, adding more layers does not help, and cannot thus solve the XOR problem (more on this later).
This realisation in the dying out interest in neural networks.

In 1958 Rosenblatt described learning of the weight values in the perceptron.

\subsubsection{1980s--1990s: Connectionism}
The new era was characterized by multi-layer networks and back-propagation.
Deep learning saw revived interest in the 1980s as connectionism arised.
The fundamental idea of connectionism is that a large number of simple units can be connected together to obtain more complex properties.
With a network having multiple layers, we encounter more complex functions to describe a neuron: a first layer of neurons would receive inputs \(x_{0}, \dots , x_{n}\), then another layer would receive these modulated by weight values \(w_{i, j}^{n}\) for each connection, finally giving \(y_{0}, \dots , y_{n}\) outputs.
For this kind of architecture, it can be expressed as:

\begin{equation*}
  y_{j} = f(\sum_{i=1}^{n} w_{i,j} x_{i}) = f(\textbf{W}^{T} \textbf{x})
\end{equation*}

By now, people had realized the importance of adding non-linearity.

The sigmoid function is an example of a non-linear function, a requirement to be able to perform backpropagation for learning.
Applying this to the previous McCulloch-Pitts model equation, we obtain:

\begin{equation*}
  y = \sigma (\sum_{i} w_{i} x_{i})
\end{equation*}


Despite all this, success in other areas pushed out neural networks again in the 1990s.

\subsubsection{2010s: Deep Learning}
The trend goes towards deeper networks, the use of GPUs, and better training techniques.

There has been a deep learning boom since the 2010s.
Despite recent hype, most of the algorithms we use are in fact old (1980s--1990s).
The innovation since then lies in improved training techniques that allow larger and deeper models.
We also produced large datasets for training networks (from approximately \(10^{2}\) to \(10^{3}\) training sampled in the past to approximately \(10^{5}\) to \(10^{6}\) is the 2010s).
we have also benefited from faster computers, in particular developments in GPUs that allow massive parallel processing.


\section{Artificial Neurons and Activations}

\subsection{Artificial Neurons}
An artificial neuron is the basic unit of neural nets.
It performs a simple computation.
For a single neuron, one or more inputs \(x_{n}\) reach modulated by the weight values \(w_{n}\), and added all together with a bias term \(b\).
The result is called the preactivation \(\textbf{w}^{\text{T}} \textbf{x} + b\), then subjected to an activation function \(f(\cdot)\) to produce an output \(y\).
Putting the equations together:

\quad \textit{compute pre-activation}
\begin{equation*}
  z = b + \sum_{i=1}^{n} w_{i} x_{i} = b + \textbf{w}^{\text{T}} \textbf{x}
\end{equation*}

\quad \textit{compute output}
\begin{equation*}
  y = f(z) = f(b + \textbf{w}^{\text{T}} \textbf{x})
\end{equation*}

\(b\) can also be seen as an additional weight with value 1 connected to a constant.
Displaying weights and inputs as vectors:

\begin{equation*}
  \textbf{w}^{\prime} = 
  \begin{bmatrix}
    w_{0} \\
    \vdots \\
    w_{n} \\
    b
  \end{bmatrix}
  ;\quad \textbf{x}^{\prime} = 
  \begin{bmatrix}
    w_{0} \\
    \vdots \\
    w_{n} \\
    1
  \end{bmatrix}
\end{equation*}

\(f(\cdot)\) is called an \emph{activation function}.
Very importantly, it brings non-linearity.
It also limits the pre-activation value to some range.
The activation funtion needs to be \emph{differentiable} for gradient-based optimization (training; explained later).

\subsection{Activation Functions}

\subsubsection{Linear}
It is the same as having no activation.
They take the form:

\begin{equation*}
  f(x) = z
\end{equation*}

\subsubsection{Step Function}
It was classically used, for example in the original perceptron.
Again it is not differentiable.
When visualized as a function of a set of input values (therefore in 3D axes), it gives a separating hyperplane.

\begin{equation*}
  f(z) = H(z) = \begin{cases}
    1 &  \text{for}~z \geq 0, \\
    0 &  \text{for}~z < 0
  \end{cases}
\end{equation*}

\subsubsection{Sigmoid}
Bounds the function values between 0 and 1.

\begin{equation*}
  f(z) = \frac{1}{1 + e^{-z}}
\end{equation*}

\subsubsection{Hyperbolic Tangent}
Bounds between -1 and 1.

\begin{equation*}
  f(z) = \text{tanh}(z) = \frac{e^{2z} - 1}{e^{2z} + 1}
\end{equation*}

\subsubsection{Rectified Linear}
ReLU\@.
Gives a value of zero for some values, and a value for others.
Tends to give sparse activities, with more outputs that are exactly zero.

\begin{equation*}
  f(z) = \text{max}(0, z)
\end{equation*}


\subsection{Classification: Single Neuron}
With only a single neuron, we can only perform binary classification.
A sigmoid activation function gives activations interpreted as \(P(y = 1 \mid \textbf{x})\), performing logistic regression.

For a problem to be compatible with binary classification, it needs to be linearly separable.
A theoretical class 0 and class 1 would be separated by a decision boundary depending on two values \(x_{1}\) and \(x_{2}\).

You can have a look at how everything of this works at \url{https://playground.tensorflow.org}.


\section{Multiple Neurons}
Performing binary classification is enough for a classification task for linearly separable logic functions.
Imagine we have the following functions:

\begin{equation*}
  f(x_{1}, x_{2}) = \text{AND}(\bar{x}_{1}, x_{2})
\end{equation*}

\quad and,

\begin{equation*}
  f(x_{1}, x_{2}) = \text{AND}(x_{1}, \bar{x}_{2})
\end{equation*}

If we start giving values of either 0 or 1 each of the functions, we would obtain two \(2 \times 2\) matrices.
Each of these would only have one element different from the others.
Both matrices would only differ in the position of that element.
In this case, it would be easy to draw a line separating the one different value from the other three.

However, let's change the logic function:

\begin{equation*}
  f(x_{1}, x_{2}) = \text{XOR}(x_{1}, {x}_{2})
\end{equation*}

If we give values to the function in the same way, we find that the two diagonals of the matrix are different.
It becomes impossible to do linear separation in this situation.
However, the two AND functions together seem to form the solution to the XOR problem, only that we indeed need two of these functions.

We see that can use two neurons to first transform the problem into a better representation.
If we were to implement XOR as a network, we would have to inputs, one is always 0 and the other is 1.
These are fed into two AND function neurons with their respective weights.
A third neuron in the output layer, the so called output neuron, is the XOR neuron.
As a result, there is a very simple problem that cannot be solved by a single neuron.
However, we can use two neurons to first transform the problem into a better representation.
It motivates the usage of multiple neurons in many layers.
The non-linear activation function is also essential.

With many neurons in layers, we can formally describe the network as:

\begin{equation*}
  y_{j} = f(\sum_{i=1}^{n} w_{i,j} x_{i})
\end{equation*}

or for more efficient matrix multiplication:

\begin{align*}
  \textbf{y} &= f \left(
  \begin{bmatrix}
    w_{1,1} & w_{1,2} & \cdots  & w_{1,n} \\
    w_{2,1} & w_{2,2} & \cdots  & w_{2,n} \\
    \vdots  & \vdots  & \ddots & \vdots  \\
    w_{m,1} & w_{m,2} & \cdots  & w_{m,n} \\
  \end{bmatrix}
  \begin{bmatrix}
    x_{1}  \\
    \vdots \\
    x_{n}  \\
  \end{bmatrix}
  \right) = \\
             &= f(\textbf{W}^{T} \textbf{x})
\end{align*}

In a feedforward network or multi-layer perceptron we have one or more hiden layers.
We find neurons in the input, hidden, and output layers:

\quad \textit{network output}
\begin{equation*}
  \textbf{y} = f(\textbf{U}^{T} \textbf{h})
\end{equation*}

\quad \textit{hidden layer output}
\begin{equation*}
  \textbf{h} = g(\textbf{W}^{T} \textbf{x})
\end{equation*}

\quad \textit{as a result:}
\begin{equation*}
  \textbf{y} = f(\textbf{U}^{T} g(\textbf{W}^{T} \textbf{x}))
\end{equation*}

Network depth is usually defined as the number of layers with tunable weights (hidden layers plus one output layer)
A non-linear activation is essential, otherwise we could simplify the functions of the many neurons into a single neuron:

\begin{equation*}
  \textbf{y} = f(\textbf{U}^{T} g(\textbf{W}^{T} \textbf{x})) = \textbf{U}^{T} \textbf{W}^{T} \textbf{x} = \textbf{V}^{T} \textbf{x}
\end{equation*}


\subsection{Output Activation}
We can have one or more output units.
The choice of output activation function is tightly coupled with the task that the network is solving.
If we are performing a regression task, then \(\hat{y} \approx f(x)\).
In a classification task \(\hat{y} = P(y = 1 \mid \textbf{x})\)
We can see output layer examples below:

\subsubsection{Single-class Classifier}
In an example with a set of images as input \textbf{x}, we want to classify if an image would contain a cat or not.
The desired output \(y\) is 1 for the target class, and 0 if not.
In this case the output layer could contain a single neuron with a sigmoid function as \(\sigma(x) = y \in \mathbb{R}_{[0,1]}\).
The interpretation of this output value is as the probability of an image depicting a cat \(P(y = 1 \mid \textbf{x})\).

\subsubsection{Multiple-class Classifier}
We could have many classes as in bird, cat, dog, horse, etc.
It is not a good idea to make an output neuron with values 0 to number of labels.
If so, label 1 would probably not more related to label 2 than to label 5, but a appear to do so.
More importantly, values with non-zero decimals cannot really be interpreted.
Data is categorical, not numerical or ordinal.
The correct approach is to have one neuron per label, with values 0 to 1 as the probability of the input to belong to that class.

\subsubsection{Selecting \(f(\cdot)\)}
It depends on the problem.
In a multi-label context the sigmoid function works well enough.
For mutually exclusive multi-class problems, the sum of the outputs sums one.
In these cases, we interpret output as a probability.
The desired output needs to be in the same form, so it will be a binary vector where 1 means class membership.

\subsection{Capacity a Neural Network}
It is based on the \emph{Universal approximation theorem} by Hornik (1989).
According to it, a feedforward network can approximate any continuous function arbitrarily well if it has:

\begin{itemize}
  \item A linear output layer
  \item At least one hidden layer with certain non-linear activaiton function
  \item Enough hidden units
\end{itemize}

However there is no guarantee that a training algorithm would be able to fing the correct weights.





\end{document}

